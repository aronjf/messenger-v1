- **Чтобы зайти в контейнер нужно прописать команду:**
  - `docker exec -t` *имя контейнера* `bash`
    

- **Чтобы зайти в cli каждого из контейнера нужно прописать уникальные команды:**
  - Для psql-container:
    - `psql -U postgres`
  - Для cassandra-container:
    - `cqlsh`
  - Для redis-container:
    - `redis-cli`
    

- **Алгоритм по деплою проекта:**
  - Нажать на build тем самым сбилдить .jar
    ![img.png](img.png)
  - Собрать image с командой `docker build -t messenger .`
  - Далее нужно прописать команду:
    - `docker-compose up`
  - Затем выполнить пункт 1, в имени контейнера указать _cassandra-container_  
  - Затем прописать 2 пункт, а именно cqlsh(cli cassandra)
  - Создание кейспейса(базы данных) - 
    `CREATE KEYSPACE messenger
    WITH REPLICATION = {
    'class' : 'SimpleStrategy',
    'replication_factor' : 1
    }; ` 
  - Заходим в кейспейс - `USE messenger;` 
  - Создаем таблицу - `create table message(
    chatRoomId text, 
    sendingDate timestamp, 
    fromUser text, 
    toUser text, 
    message text, 
    primary key (chatRoomId, sendingDate)) 
    with clustering order by (sendingDate ASC);`
    
P.S. К сожалению нихуя не получилось сделать миграцию кейспейса и кассандры,
но между тем psql бд, таблицы и необходимые и тестовые данные подгружает автоматически
