FROM openjdk:16

ENV PORT 3031
EXPOSE $PORT

ARG DIR=build/libs/messenger-0.0.1-SNAPSHOT.jar

COPY $DIR messenger.jar

ENTRYPOINT java -jar messenger.jar