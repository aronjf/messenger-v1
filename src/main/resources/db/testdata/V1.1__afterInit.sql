insert into role_
values(1, 'USER')
on conflict do nothing;

insert into role_
values(2, 'ADMIN')
on conflict do nothing;

insert into role_
values(3, 'CREATOR')
on conflict do nothing;

insert into user_
values(1,
       'nikitaron',
       'nikitaron@gmail.com',
       '$2a$10$wS04l3hcbzslzIA2TOntf.z28TChcPTjLm0ndojQnj7SofLXMaqVa', --<n26012 password
       true)
on conflict do nothing;
select nextval('user__id_seq'::regclass);

insert into users_role
values(1, 2)
on conflict do nothing;

insert into country_of_residence
values(
       1,
       'Belarus',
       'Minsk',
       false
)
on conflict do nothing;
select nextval('country_of_residence_id_seq'::regclass);
select nextval('media_profile_image__id_seq'::regclass);

insert into media_about_information(id, date_of_birth, country_of_residence_id)
values(
      1,
      '12 01 2003',
      1
)
on conflict do nothing;
select nextval('media_about_information_id_seq'::regclass);

insert into media_profile_image(id)
values(1)
on conflict do nothing;
select nextval('media_profile_image__id_seq'::regclass);

insert into media_profile
values(1, 1, 1, 1)
on conflict do nothing;