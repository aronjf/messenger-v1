create table user_(
                      id       bigserial          not null,
                      username varchar(255)  not null unique,
                      email    varchar(255)  not null,
                      password varchar(255)  not null,
                      is_enabled boolean     not null default false,
                      primary key(id)
);

create table role_(
                      id        smallint     not null,
                      role_name varchar(255) not null,
                      primary key(id)
);

create table users_role(
                      user_id bigserial      references user_(id) not null,
                      role_id smallint       references role_(id) not null
);

create table country_of_residence(
                                     id bigserial not null,
                                     country      varchar(50),
                                     town         varchar(50),
                                     is_empty boolean default false not null,
                                     primary key(id)
);

create table media_about_information(
                                        id                     bigserial   not null,
                                        date_of_birth          varchar(255),
                                        about_user             varchar(255) default 'The user has chosen not to leave any mention of himself',
                                        country_of_residence_id bigserial references country_of_residence(id) not null,
                                        primary key(id)
);

create sequence media_profile_image__id_seq start 1 increment 1;

create table media_profile_image(
                                    id                 smallint default nextval('media_profile_image__id_seq') not null,
                                    image_name         varchar(255) default '720991618',
                                    date_of_image      timestamp default current_timestamp,
                                    primary key(id)
);

create table media_profile(
                      id                   bigserial   not null,
                      user_id              bigserial   references user_(id)                   not null,
                      image_id             smallint    references media_profile_image(id)     not null,
                      about_information_id bigserial   references media_about_information(id) not null,
                      primary key(id)
);