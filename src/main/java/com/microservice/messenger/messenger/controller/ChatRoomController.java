package com.microservice.messenger.messenger.controller;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.config.RoleExpression;
import com.microservice.messenger.auth.exception.ChatRoomNotFoundException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.messenger.annotation.LogbackMessage;
import com.microservice.messenger.messenger.dto.ChatRoomDto;
import com.microservice.messenger.messenger.dto.MessageDto;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.Message;
import com.microservice.messenger.messenger.service.ChatRoomService;
import com.microservice.messenger.messenger.service.MessageService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.security.Principal;

@RestController
@RequestMapping("/api/chatroom/")
@AllArgsConstructor
public class ChatRoomController {

    private final ChatRoomService chatRoomService;
    private final MessageService messageService;
    private final ConversionService conversionService;

    @Logback
    @SneakyThrows
    @GetMapping("create")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<ChatRoomDto> createChatRoom(@Valid @RequestBody ChatRoomDto chatRoomDto, Principal principal){
        return new ResponseEntity<>(conversionService.convert(
                chatRoomService.saveChatRoom(conversionService.convert(chatRoomDto, ChatRoom.class), principal.getName()), ChatRoomDto.class),
                HttpStatus.CREATED);
    }

    @Logback
    @DeleteMapping("drop/{chatRoomName}")
    @PreAuthorize(RoleExpression.AUTHORITY_CREATOR)
    @ResponseStatus(HttpStatus.OK)
    public void dropChatRoom(@PathVariable String chatRoomName, Principal principal) throws ChatRoomNotFoundException {
        chatRoomService.dropChatRoom(chatRoomName, principal.getName());
    }

    @LogbackMessage
    @GetMapping("join/{chatRoomId}")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<ChatRoomDto> joinChatRoom(@PathVariable String chatRoomId) throws EntityNotFoundException {
        return new ResponseEntity<>(conversionService.convert(
                chatRoomService.findChatRoomById(chatRoomId), ChatRoomDto.class), HttpStatus.OK);
    }

    @LogbackMessage
    @PostMapping("send")
    public void sendMessage(@Valid @RequestBody MessageDto messageDto, @RequestHeader String chatRoomId, Principal principal)
            throws EntityNotFoundException {

        messageDto.setChatRoomId(chatRoomId);
        messageDto.setFromUser(principal.getName());
        messageDto.setToUser(chatRoomService.findChatRoomById(chatRoomId).getChatRoomName());

        messageService.sendMessageToChatRoom(conversionService.convert(messageDto, Message.class));
    }
}
