package com.microservice.messenger.messenger.config;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import com.microservice.messenger.messenger.service.ChatRoomService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import java.util.Objects;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class MessageListener {

    public static final String CHATROOM_HEADER = "chatRoomId";

    private final ChatRoomService chatRoomService;

    @Logback
    @EventListener
    public void handleSessionConnected(SessionConnectEvent event) throws EntityNotFoundException {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());

        var chatRoomId = Objects.requireNonNull(headers.getNativeHeader(CHATROOM_HEADER)).get(0);

        Objects.requireNonNull(headers.getSessionAttributes()).put(CHATROOM_HEADER, chatRoomId);

        ChatRoomUser chatRoomUser = ChatRoomUser.builder()
                .username(event.getUser().getName())
                .build();

        chatRoomService.joinChatRoom(chatRoomUser, chatRoomService.findChatRoomById(chatRoomId));
    }

    @Logback
    @EventListener
    public void handleSessionDisconnect(SessionDisconnectEvent event) throws EntityNotFoundException {
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());

        String chatRoomId = Objects.requireNonNull(headers.getSessionAttributes()).get(CHATROOM_HEADER).toString();

        ChatRoomUser chatRoomUser = ChatRoomUser.builder()
                .username(event.getUser().getName())
                .build();

        chatRoomService.leftChatRoom(chatRoomUser, chatRoomService.findChatRoomById(chatRoomId));
    }
}
