package com.microservice.messenger.messenger.converter;

import com.microservice.messenger.messenger.dto.ChatRoomDto;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ChatRoomToChatRoomDtoConverter implements Converter<ChatRoom, ChatRoomDto> {

    private final ModelMapper modelMapper;

    @Override
    public ChatRoomDto convert(ChatRoom source) {
        return modelMapper.map(source, ChatRoomDto.class);
    }
}
