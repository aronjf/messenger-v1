package com.microservice.messenger.messenger.converter;

import com.microservice.messenger.messenger.dto.MessageDto;
import com.microservice.messenger.messenger.model.entity.Message;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class MessageDtoToMessageConverter implements Converter<MessageDto, Message> {

    private final ModelMapper modelMapper;

    @Override
    public Message convert(MessageDto source) {
        return modelMapper.map(source, Message.class);
    }
}
