package com.microservice.messenger.messenger.converter;

import com.microservice.messenger.messenger.dto.ChatRoomDto;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ChatRoomDtoToChatRoomConverter implements Converter<ChatRoomDto, ChatRoom> {

    private final ModelMapper modelMapper;

    @Override
    public ChatRoom convert(ChatRoomDto source) {
        return modelMapper.map(source, ChatRoom.class);
    }
}
