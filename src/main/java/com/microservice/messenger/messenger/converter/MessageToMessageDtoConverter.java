package com.microservice.messenger.messenger.converter;

import com.microservice.messenger.messenger.dto.MessageDto;
import com.microservice.messenger.messenger.model.entity.Message;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class MessageToMessageDtoConverter implements Converter<Message, MessageDto> {

    private final ModelMapper modelMapper;

    @Override
    public MessageDto convert(Message source) {
        return modelMapper.map(source, MessageDto.class);
    }
}
