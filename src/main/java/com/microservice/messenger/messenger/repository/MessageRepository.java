package com.microservice.messenger.messenger.repository;

import com.microservice.messenger.messenger.model.entity.Message;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends CassandraRepository<Message, String> {
}
