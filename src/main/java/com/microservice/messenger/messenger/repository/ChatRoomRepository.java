package com.microservice.messenger.messenger.repository;

import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChatRoomRepository extends CrudRepository<ChatRoom, String> {

    Optional<ChatRoom> findChatRoomByChatRoomName(String chatRoomName);

    ChatRoom findByConnectedUsers(List<ChatRoomUser> connectedUser);
}
