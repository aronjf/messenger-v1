package com.microservice.messenger.messenger.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.core.cql.Ordering;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.time.LocalDateTime;

@Table
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Message {

    @PrimaryKeyColumn(name = "chatroomid", ordinal = 1, type = PrimaryKeyType.PARTITIONED)
    private String chatRoomId;

    private String message;

    @Column(value = "fromuser")
    private String fromUser;

    @Column(value = "touser")
    private String toUser;

    @Transient
    @PrimaryKeyColumn(name = "sendingdate", ordinal = 2, type = PrimaryKeyType.CLUSTERED, ordering = Ordering.ASCENDING)
    @Builder.Default
    private LocalDateTime sendingDate = LocalDateTime.now();
}
