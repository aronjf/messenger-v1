package com.microservice.messenger.messenger.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//TODO maybe would be better to save a user entity, not username
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomUser{

    private String username;

    @Builder.Default
    private LocalDateTime joinAt = LocalDateTime.now();
}
