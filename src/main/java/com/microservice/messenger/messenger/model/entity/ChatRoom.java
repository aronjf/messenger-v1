package com.microservice.messenger.messenger.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@RedisHash("chatroom")
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoom {

    @Id
    private String id;

    private String chatRoomName;

    @Builder.Default
    private List<ChatRoomUser> connectedUsers = new ArrayList<>();
}
