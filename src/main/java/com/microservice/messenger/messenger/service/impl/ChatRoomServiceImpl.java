package com.microservice.messenger.messenger.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.ChatRoomNotFoundException;
import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.service.RoleService;
import com.microservice.messenger.auth.service.UserService;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import com.microservice.messenger.messenger.repository.ChatRoomRepository;
import com.microservice.messenger.messenger.service.ChatRoomService;
import com.microservice.messenger.messenger.service.ChatRoomUserService;
import com.microservice.messenger.messenger.service.MessageService;
import com.microservice.messenger.messenger.service.util.Destination;
import com.microservice.messenger.messenger.service.util.NotificationMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.microservice.messenger.auth.model.Roles.CREATOR;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ChatRoomServiceImpl implements ChatRoomService{

    private final SimpMessagingTemplate simpMessagingTemplate;
    private MessageService messageService;
    private final ChatRoomUserService chatRoomUserService;
    private final ChatRoomRepository chatRoomRepository;
    private final UserService userService;
    private final RoleService roleService;

    @Logback
    @Override
    public ChatRoom saveChatRoom(ChatRoom chatRoom, String username)
            throws EntityExistException, EntityNotFoundException {

        if(chatRoomRepository.findChatRoomByChatRoomName(chatRoom.getChatRoomName()).isPresent())
            throw new EntityExistException(chatRoom.getChatRoomName());

        ChatRoomUser chatRoomUser = chatRoomUserService.findUserByUsernameAndBuildChatRoomUser(username);

        var user = userService.findUserByUsername(chatRoomUser.getUsername());

        user.getRoles().add(roleService.findRoleByName(CREATOR.name()));

        userService.saveUser(user);

        return chatRoomRepository.save(
                ChatRoom.builder()
                .chatRoomName(chatRoom.getChatRoomName())
                .connectedUsers(Collections.singletonList(chatRoomUser))
                .build()
        );
    }

    @Logback
    @Override
    public void dropChatRoom(String chatRoomName, String username) throws ChatRoomNotFoundException {

        var chatRoomUser = chatRoomUserService.findUserByUsernameAndBuildChatRoomUser(username);

        var chatRoom = chatRoomRepository.findByConnectedUsers(Collections.singletonList(chatRoomUser));

        if(Optional.of(chatRoom).isEmpty())
            throw new ChatRoomNotFoundException(chatRoomUser.getUsername());

        chatRoomRepository.delete(chatRoom);
    }

    @Logback
    @Override
    public ChatRoom joinChatRoom(ChatRoomUser chatRoomUser, ChatRoom chatRoom) throws EntityNotFoundException {

        var updatedConnectedUsers = new ArrayList<>(chatRoom.getConnectedUsers());

        updatedConnectedUsers.add(chatRoomUser);

        var updatedChatRoom = ChatRoom.builder()
                .chatRoomName(chatRoom.getChatRoomName())
                .connectedUsers(updatedConnectedUsers)
                .build();

        messageService.sendMessageToChatRoom(NotificationMessage.userJoined(chatRoomUser.getUsername(), updatedChatRoom));
        updateConnectedUsers(updatedChatRoom);

        return chatRoomRepository.save(updatedChatRoom);
    }

    @Logback
    @Override
    public ChatRoom leftChatRoom(ChatRoomUser chatRoomUser, ChatRoom chatRoom) throws EntityNotFoundException {
        var updatedConnectedUsers = new ArrayList<>(chatRoom.getConnectedUsers());

        updatedConnectedUsers.remove(chatRoomUser);

        var updatedChatRoom = ChatRoom.builder()
                .chatRoomName(chatRoom.getChatRoomName())
                .connectedUsers(updatedConnectedUsers)
                .build();

        messageService.sendMessageToChatRoom(NotificationMessage.userLeft(chatRoomUser.getUsername(), updatedChatRoom));
        updateConnectedUsers(updatedChatRoom);

        return chatRoomRepository.save(updatedChatRoom);
    }

    @Logback
    @Override
    public ChatRoom findChatRoomById(String chatRoomId) throws EntityNotFoundException {
        var chatRoom = chatRoomRepository.findById(chatRoomId);

        if(chatRoom.isEmpty())
            throw new EntityNotFoundException(chatRoomId);

        return chatRoom.get();
    }

    @Logback
    private void updateConnectedUsers(ChatRoom chatRoom) {
        simpMessagingTemplate.convertAndSend(
                Destination.connectedUsers(chatRoom.getId()),
                chatRoom.getConnectedUsers());
    }
}
