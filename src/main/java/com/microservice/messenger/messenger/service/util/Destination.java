package com.microservice.messenger.messenger.service.util;

public class Destination {

    public static final String PUBLIC_TOPIC = "/topic/%s.public.messages";
    public static final String PRIVATE_QUEUE = "/queue/%s.private.messages";
    public static final String CONNECTED_USERS = "/topic/%s.connected.users";

    public static String publicMessages(String chatRoomId) {
        return String.format(PUBLIC_TOPIC, chatRoomId);
    }

    public static String privateMessages(String chatRoomId) {
        return String.format(PRIVATE_QUEUE, chatRoomId);
    }

    public static String connectedUsers(String chatRoomId) {
        return String.format(CONNECTED_USERS, chatRoomId);
    }
}
