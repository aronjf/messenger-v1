package com.microservice.messenger.messenger.service;

import com.microservice.messenger.auth.exception.ChatRoomNotFoundException;
import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;

public interface ChatRoomService {

    ChatRoom saveChatRoom(ChatRoom chatRoom, String username)
            throws EntityExistException, EntityNotFoundException;

    void dropChatRoom(String chatRoomName, String username)
            throws ChatRoomNotFoundException;

    ChatRoom joinChatRoom(ChatRoomUser chatRoomUser, ChatRoom chatRoom) throws EntityNotFoundException;

    ChatRoom leftChatRoom(ChatRoomUser chatRoomUser, ChatRoom chatRoom) throws EntityNotFoundException;

    ChatRoom findChatRoomById(String chatRoomId) throws EntityNotFoundException;
}
