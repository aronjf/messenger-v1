package com.microservice.messenger.messenger.service.util;

import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.Message;

public class NotificationMessage {

    public static final String USER_JOINED_CHAT = "User %s joined";
    public static final String USER_LEFT_CHAT = "User %s left";

    public static Message userJoined(String username, ChatRoom chatRoom){
        return Message.builder()
                .fromUser(chatRoom.getChatRoomName())
                .toUser(chatRoom.getConnectedUsers().toString())
                .message(String.format(USER_JOINED_CHAT, username))
                .build();
    }

    public static Message userLeft(String username, ChatRoom chatRoom){
        return Message.builder()
                .fromUser(chatRoom.getChatRoomName())
                .toUser(chatRoom.getConnectedUsers().toString())
                .message(String.format(USER_LEFT_CHAT, username))
                .build();
    }
}
