package com.microservice.messenger.messenger.service;

import com.microservice.messenger.messenger.model.entity.ChatRoomUser;

public interface ChatRoomUserService {

    ChatRoomUser findUserByUsernameAndBuildChatRoomUser(String username);
}
