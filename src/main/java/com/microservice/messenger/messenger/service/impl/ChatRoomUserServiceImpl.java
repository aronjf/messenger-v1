package com.microservice.messenger.messenger.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import com.microservice.messenger.messenger.service.ChatRoomUserService;
import org.springframework.stereotype.Service;

@Service
public class ChatRoomUserServiceImpl implements ChatRoomUserService {

    @Logback
    @Override
    public ChatRoomUser findUserByUsernameAndBuildChatRoomUser(String username){
        return ChatRoomUser.builder()
                .username(username)
                .build();
    }
}
