package com.microservice.messenger.messenger.service;

import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.messenger.model.entity.Message;

public interface MessageService {

    void sendMessageToChatRoom(Message message) throws EntityNotFoundException;
}
