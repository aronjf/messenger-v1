package com.microservice.messenger.messenger.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.messenger.model.entity.Message;
import com.microservice.messenger.messenger.service.ChatRoomService;
import com.microservice.messenger.messenger.service.MessageService;
import com.microservice.messenger.messenger.service.util.Destination;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MessageServiceImpl implements MessageService {

    private final CassandraRepository<Message, String> cassandraRepository;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ChatRoomService chatRoomService;

    @Logback
    @Override
    public void sendMessageToChatRoom(Message message) throws EntityNotFoundException {
        simpMessagingTemplate.convertAndSend(
                Destination.publicMessages(message.getChatRoomId()), message);

        sendMessageToDialog(message);
    }

    @Logback
    //TODO did pjql query instead forEach
    private void sendMessageToDialog(Message message) throws EntityNotFoundException {
        var chatRoom = chatRoomService.findChatRoomById(message.getChatRoomId());

        chatRoom.getConnectedUsers().forEach(u -> {
            message.setToUser(u.getUsername());

            saveMessage(message);
        });
    }

    @Logback
    private void saveMessage(Message message) {
        cassandraRepository.save(Objects.requireNonNull(message));
    }
}
