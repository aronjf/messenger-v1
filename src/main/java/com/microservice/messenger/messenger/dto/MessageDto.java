package com.microservice.messenger.messenger.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MessageDto {

    public static final String DATE_PATTERN = "yyyy-MM-dd HH:mm";

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String chatRoomId;

    @Min(1)
    private String message;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY, value = "messageId")
    private String fromUser;

    private String toUser;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = DATE_PATTERN)
    private LocalDateTime sendingDate = LocalDateTime.now();
}
