package com.microservice.messenger.messenger.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChatRoomDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY, value = "chatRoomId")
    private String id;

    @Size(
            min = 2,
            max = 18,
            message = "{warnings.validation.chatroom.name}"
    )
    private String chatRoomName;

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private List<ChatRoomUser> connectedUser = new ArrayList<>();
}
