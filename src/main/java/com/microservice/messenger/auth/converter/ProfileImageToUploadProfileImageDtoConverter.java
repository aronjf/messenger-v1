package com.microservice.messenger.auth.converter;

import com.microservice.messenger.auth.dto.UploadProfileImageDto;
import com.microservice.messenger.auth.model.entity.ProfileImage;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ProfileImageToUploadProfileImageDtoConverter implements Converter<UploadProfileImageDto, ProfileImage> {

    private final ModelMapper modelMapper;

    @Override
    public ProfileImage convert(UploadProfileImageDto source) {
        return modelMapper.map(source, ProfileImage.class);
    }
}
