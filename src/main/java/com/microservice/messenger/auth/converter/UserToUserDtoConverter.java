package com.microservice.messenger.auth.converter;

import com.microservice.messenger.auth.dto.UserDto;
import com.microservice.messenger.auth.model.entity.User;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class UserToUserDtoConverter implements Converter<User, UserDto> {

    private final ModelMapper modelMapper;

    @Override
    public UserDto convert(User source) {
        return modelMapper.map(source, UserDto.class);
    }
}
