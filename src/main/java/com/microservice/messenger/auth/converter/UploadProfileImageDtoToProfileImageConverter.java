package com.microservice.messenger.auth.converter;

import com.microservice.messenger.auth.dto.UploadProfileImageDto;
import com.microservice.messenger.auth.model.entity.ProfileImage;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class UploadProfileImageDtoToProfileImageConverter implements Converter<ProfileImage, UploadProfileImageDto> {

    private final ModelMapper modelMapper;

    @Override
    public UploadProfileImageDto convert(ProfileImage source) {
        return modelMapper.map(source, UploadProfileImageDto.class);
    }
}
