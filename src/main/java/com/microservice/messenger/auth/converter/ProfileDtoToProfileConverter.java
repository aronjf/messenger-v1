package com.microservice.messenger.auth.converter;

import com.microservice.messenger.auth.dto.ProfileDto;
import com.microservice.messenger.auth.model.entity.Profile;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class ProfileDtoToProfileConverter implements Converter<ProfileDto, Profile> {

    private final ModelMapper modelMapper;

    @Override
    public Profile convert(ProfileDto source) {
        return modelMapper.map(source, Profile.class);
    }
}
