package com.microservice.messenger.auth.exception;

public class JwtTokenNotFoundException extends RuntimeException {

    public static final String TOKEN_NOT_FOUND = "Token %s not found in redis database";

    public JwtTokenNotFoundException(String token){
        super(String.format(TOKEN_NOT_FOUND, token));
    }
}
