package com.microservice.messenger.auth.exception;

public class RequiredMediaTypePngException extends RuntimeException {

    public static final String REQUIRED_FILE_TYPE_PNG = "File %s with media type %s should be PNG";

    public RequiredMediaTypePngException(String filename, String mediaType){
        super(String.format(REQUIRED_FILE_TYPE_PNG, filename, mediaType));
    }
}
