package com.microservice.messenger.auth.exception;

public class EntityNotFoundException extends RuntimeException {

    public static final String ENTITY_NOT_FOUND = "Entity %s not found";

    public EntityNotFoundException(Object principal){
        super(String.format(ENTITY_NOT_FOUND, principal));
    }
}
