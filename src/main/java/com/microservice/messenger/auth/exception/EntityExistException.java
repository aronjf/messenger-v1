package com.microservice.messenger.auth.exception;

public class EntityExistException extends RuntimeException {

    public static final String ENTITY_EXIST = "Entity %s already exist";

    public EntityExistException(Object principal){
        super(String.format(ENTITY_EXIST, principal));
    }
}
