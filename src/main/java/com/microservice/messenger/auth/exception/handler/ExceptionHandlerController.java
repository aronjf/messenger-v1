package com.microservice.messenger.auth.exception.handler;

import com.microservice.messenger.auth.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController {

    @ExceptionHandler(ChatRoomNotFoundException.class)
    public ResponseEntity<String> chatRoomNotFoundHandler(ChatRoomNotFoundException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(EntityExistException.class)
    public ResponseEntity<String> entityExistHandler(EntityExistException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<String> entityNotFoundHandler(EntityNotFoundException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(JwtTokenNotFoundException.class)
    public ResponseEntity<String> jwtTokenNotFoundHandler(JwtTokenNotFoundException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(JwtTokenNotValidException.class)
    public ResponseEntity<String> jwtTokenNotValidHandler(JwtTokenNotValidException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RequiredMediaTypePngException.class)
    public ResponseEntity<String> requiredMediaTypePngHandler(RequiredMediaTypePngException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SendMailException.class)
    public ResponseEntity<String> sendMailHandler(SendMailException e){
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
