package com.microservice.messenger.auth.exception;

public class SendMailException extends RuntimeException {

    public static final String FAILED_WHILE_TRYING_TO_SEND_MAIL = "Email %s not found or mail send service not responding";

    public SendMailException(String email){
        super(String.format(FAILED_WHILE_TRYING_TO_SEND_MAIL, email));
    }
}
