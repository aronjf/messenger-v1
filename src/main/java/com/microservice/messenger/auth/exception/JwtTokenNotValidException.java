package com.microservice.messenger.auth.exception;

public class JwtTokenNotValidException extends RuntimeException {

    public static final String TOKEN_NOT_VALID = "Token %s not valid";

    public JwtTokenNotValidException(String token){
        super(String.format(TOKEN_NOT_VALID, token));
    }
}
