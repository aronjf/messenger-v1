package com.microservice.messenger.auth.exception;

public class ChatRoomNotFoundException extends RuntimeException {

    public static final String CHAT_ROOM_NOT_FOUND = "Chat room with user %s not found";

    public ChatRoomNotFoundException(String chatRoomUserName){
        super(String.format(CHAT_ROOM_NOT_FOUND, chatRoomUserName));
    }
}
