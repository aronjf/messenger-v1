package com.microservice.messenger.auth.controller;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.config.RoleExpression;
import com.microservice.messenger.auth.dto.UserDto;
import com.microservice.messenger.auth.service.MediaService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("/api/media/")
public class MediaController {

    private final MediaService mediaService;
    private final ConversionService conversionService;

    @Logback
    @GetMapping("online")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<List<UserDto>> onlineUser() {
                return new ResponseEntity<>(
                        mediaService.getOnlineUsers()
                        .stream()
                        .map(s -> conversionService.convert(s, UserDto.class))
                        .collect(Collectors.toList()), HttpStatus.OK);
    }
}
