package com.microservice.messenger.auth.controller;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.config.RoleExpression;
import com.microservice.messenger.auth.dto.ProfileDto;
import com.microservice.messenger.auth.dto.UploadProfileImageDto;
import com.microservice.messenger.auth.dto.UserDto;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.Profile;
import com.microservice.messenger.auth.model.entity.ProfileImage;
import com.microservice.messenger.auth.service.ProfileImageService;
import com.microservice.messenger.auth.service.ProfileService;
import com.microservice.messenger.auth.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.security.Principal;

@RestController
@AllArgsConstructor
@RequestMapping("/api/media/profile/")
public class MediaProfileController {

    private final ProfileService profileService;
    private final ProfileImageService profileImageService;
    private final UserService userService;
    private final ConversionService conversionService;

    @Logback
    @GetMapping("{userId}")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<ProfileDto> userProfile(@PathVariable Long userId) throws EntityNotFoundException, IOException {
        return new ResponseEntity<>(conversionService.convert(
                profileService.findProfileByUserId(userId), ProfileDto.class), HttpStatus.OK);
    }

    @Logback
    @GetMapping("me")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<ProfileDto> usersProfile(Principal principal) throws EntityNotFoundException, IOException {
        return new ResponseEntity<>(conversionService.convert(
                profileService.findProfileByUsername(principal.getName()),ProfileDto.class), HttpStatus.OK);
    }

    @Logback
    @PutMapping("me")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<ProfileDto> updateUsersProfile(@Valid @RequestBody ProfileDto profileDto, Principal principal) throws EntityNotFoundException, IOException {
        profileDto.setUser(conversionService.convert(userService.findUserByUsername(principal.getName()), UserDto.class));

        return new ResponseEntity<>(conversionService.convert(
                profileService.updateProfile(conversionService.convert(profileDto, Profile.class)), ProfileDto.class), HttpStatus.OK);
    }

    @Logback
    @PostMapping("upload")
    @PreAuthorize(RoleExpression.AUTHENTICATED)
    public ResponseEntity<UploadProfileImageDto> uploadImage(@Valid UploadProfileImageDto uploadProfileImageDto) throws IOException {
        return new ResponseEntity<>(conversionService.convert(
                profileImageService.saveImage(conversionService.convert(
                        uploadProfileImageDto, ProfileImage.class)), UploadProfileImageDto.class),
                HttpStatus.OK);
    }
}
