package com.microservice.messenger.auth.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.config.RoleExpression;
import com.microservice.messenger.auth.dto.UserDto;
import com.microservice.messenger.auth.service.MailSenderService;
import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.exception.JwtTokenNotValidException;
import com.microservice.messenger.auth.model.entity.ConfirmationToken;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.service.ConfirmationTokenService;
import com.microservice.messenger.auth.service.RegistrationService;
import com.microservice.messenger.auth.service.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import java.io.IOException;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
@RequestMapping("/api/user/")
public class RegistrationController {

    private final RegistrationService registrationService;
    private final TokenService tokenService;
    private final ConversionService conversionService;
    private final ConfirmationTokenService confirmationTokenService;
    private final MailSenderService mailSender;

    @Logback
    @PostMapping("registration")
    @PreAuthorize(RoleExpression.PERMIT_ALL)
    public ResponseEntity<UserDto> registration(@RequestBody @Valid UserDto userDto)
            throws EntityExistException {

        ConfirmationToken token = confirmationTokenService.saveConfirmationToken(userDto.getEmail());

        mailSender.send(userDto.getEmail(),
                mailSender.buildEmailText(userDto.getUsername(), token.getId()));

        return new ResponseEntity<>(conversionService.convert(
                registrationService.saveUser(conversionService.convert(userDto, User.class)), UserDto.class),
                HttpStatus.ACCEPTED);
    }

    @Logback
    @GetMapping("registration/{token}")
    @ResponseStatus(HttpStatus.OK)
    public void authConfirm(@PathVariable("token") String token)
            throws EntityNotFoundException {

        ConfirmationToken tempToken = confirmationTokenService.findConfirmationTokenById(token);
        registrationService.verifyUser(tempToken.getAssociatedEmail());
    }

    @Logback
    @GetMapping("refresh")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @PreAuthorize(RoleExpression.PERMIT_ALL)
    public void refreshToken(HttpServletRequest request, HttpServletResponse response)
            throws JwtTokenNotValidException, EntityNotFoundException, IOException {

        String authHeader = request.getHeader(AUTHORIZATION);
        response.setContentType(APPLICATION_JSON_VALUE);

        new ObjectMapper().writeValue(response.getOutputStream(),
                tokenService.checkFilter(authHeader, request.getRequestURL().toString()));
    }
}
