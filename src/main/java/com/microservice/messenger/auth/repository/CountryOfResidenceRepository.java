package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.CountryOfResidence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface CountryOfResidenceRepository extends JpaRepository<CountryOfResidence, Long> {

    @Modifying
    @Transactional
    @Query("UPDATE CountryOfResidence c SET " +
            "c.country = CASE WHEN :#{#countryOfResidence.country} IS NOT NULL THEN :#{#countryOfResidence.country} ELSE c.country END, " +
            "c.town = CASE WHEN :#{#countryOfResidence.town} IS NOT NULL THEN :#{#countryOfResidence.town} ELSE c.town END " +
            "WHERE c.countryOfResidenceId = :#{#countryOfResidence.countryOfResidenceId}")
    void updateCountryOfResidence(@Param("countryOfResidence") CountryOfResidence countryOfResidence);

    @Modifying
    @Transactional
    @Query("UPDATE CountryOfResidence c SET c.isEmpty = CASE WHEN c.country IS NOT NULL OR c.town IS NOT NULL THEN false ELSE true END " +
            "WHERE c.countryOfResidenceId = :countryOfResidenceId")
    void updateIsEmptyStatus(@Param("countryOfResidenceId") Long countryOfResidenceId);
}
