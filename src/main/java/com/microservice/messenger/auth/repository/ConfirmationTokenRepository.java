package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.ConfirmationToken;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {

    Optional<ConfirmationToken> findById(String confirmationToken);
}
