package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.ProfileImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileImageRepository extends JpaRepository<ProfileImage, Integer> {
}
