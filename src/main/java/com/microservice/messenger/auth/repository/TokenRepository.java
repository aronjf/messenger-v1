package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.Token;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TokenRepository extends CrudRepository<Token, String> {

    Optional<Token> findById(String id);
}
