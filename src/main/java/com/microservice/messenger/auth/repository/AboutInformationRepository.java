package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.AboutInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface AboutInformationRepository extends JpaRepository<AboutInformation, Long> {

    @Modifying
    @Transactional
    @Query("UPDATE AboutInformation a SET " +
            "a.aboutUser = CASE WHEN :#{#aboutInformation.aboutUser} IS NOT NULL THEN :#{#aboutInformation.aboutUser} ELSE a.aboutUser END, " +
            "a.dateOfBirth = CASE WHEN :#{#aboutInformation.dateOfBirth} IS NOT NULL THEN :#{#aboutInformation.dateOfBirth} ELSE a.dateOfBirth END " +
            "WHERE a.aboutId = :#{#aboutInformation.aboutId}")
    void updateAboutInformation(@Param("aboutInformation") AboutInformation aboutInformation);
}
