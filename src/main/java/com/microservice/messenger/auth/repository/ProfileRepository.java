package com.microservice.messenger.auth.repository;

import com.microservice.messenger.auth.model.entity.Profile;
import com.microservice.messenger.auth.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Long> {

    Profile findByUser(User user);
}
