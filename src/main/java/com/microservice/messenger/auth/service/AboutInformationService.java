package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.AboutInformation;

public interface AboutInformationService {

    void saveAboutInformation(AboutInformation aboutInformation);

    void updateAboutInformation(AboutInformation aboutInformation);
}
