package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.model.entity.AboutInformation;
import com.microservice.messenger.auth.repository.AboutInformationRepository;
import com.microservice.messenger.auth.service.AboutInformationService;
import com.microservice.messenger.auth.service.CountryOfResidenceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AboutInformationServiceImpl implements AboutInformationService {

    private final AboutInformationRepository aboutInformationRepository;
    private final CountryOfResidenceService countryOfResidenceService;

    @Logback
    @Override
    public void saveAboutInformation(AboutInformation aboutInformation) {
        countryOfResidenceService.saveCountryOfResidence(aboutInformation.getCountryOfResidence());
        aboutInformationRepository.save(aboutInformation);
    }

    @Logback
    @Override
    public void updateAboutInformation(AboutInformation aboutInformation) {
        countryOfResidenceService.updateCountryOfResidence(aboutInformation.getCountryOfResidence());
        aboutInformationRepository.updateAboutInformation(aboutInformation);
    }
}
