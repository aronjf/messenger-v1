package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.service.ImageService;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;

@Service
public class ImageServiceImpl implements ImageService {

    @Value("${messenger-v1.path}")
    String path;

    @Value("${messenger-v1.width}")
    int width;

    @Value("${messenger-v1.height}")
    int height;

    @Logback
    @Override
    public Resource uploadImage(MultipartFile multipartFile) throws IOException {
        var outputStream = new ByteArrayOutputStream();

        var absolutePath = Paths.get(path).toAbsolutePath();

        if(!Files.exists(absolutePath))
            Files.createDirectories(absolutePath);

        Thumbnails.of(multipartFile.getInputStream())
                .size(width, height)
                .toOutputStream(outputStream);

        var inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        var hashcodeFileName =
                String.valueOf(
                        Objects.requireNonNull(
                                multipartFile.getOriginalFilename()).hashCode()
                );

        var absolutePathOfImage = absolutePath.resolve(hashcodeFileName);

        Files.copy(inputStream, absolutePathOfImage, StandardCopyOption.REPLACE_EXISTING);

        return downloadImage(hashcodeFileName);
    }

    @Logback
    @Override
    public Resource downloadImage(String fileName) throws MalformedURLException {
        var absolutePathOfImage = Paths.get(path).toAbsolutePath().resolve(fileName).toUri();

        return new UrlResource(absolutePathOfImage);
    }
}
