package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.User;

public interface RegistrationService {

    User saveUser(User user) throws EntityExistException;

    void verifyUser(String email) throws EntityNotFoundException;
}
