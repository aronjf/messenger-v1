package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.Profile;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.repository.ProfileRepository;
import com.microservice.messenger.auth.service.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@AllArgsConstructor
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;
    private final ProfileImageService profileImageService;
    private final AboutInformationService aboutInformationService;
    private final UserService userService;
    private final ImageService imageService;

    @Logback
    @Override
    public Profile findProfileByUser(User user) {
        return profileRepository.findByUser(user);
    }

    @Logback
    @Override
    public void saveProfile(Profile profile) {
        profileImageService.saveProfileImage(profile.getProfileImage());
        aboutInformationService.saveAboutInformation(profile.getAboutInformation());

        profileRepository.save(profile);
    }

    @Logback
    @Override
    public Profile updateProfile(Profile profile) throws IOException {
        aboutInformationService.updateAboutInformation(profile.getAboutInformation());

        var user = profile.getUser();

        var updatedProfile = findProfileByUser(user);

        return setProfileImageURL(updatedProfile);
    }

    @Logback
    @Override
    public Profile findProfileByUserId(Long userId) throws EntityNotFoundException, IOException {
        var user = userService.findUserByUserId(userId);

        var profile = findProfileByUser(user);

        return setProfileImageURL(profile);
    }

    @Logback
    @Override
    public Profile findProfileByUsername(String username) throws EntityNotFoundException, IOException {
        var user = userService.findUserByUsername(username);

        var profile =  findProfileByUser(user);

        return setProfileImageURL(profile);
    }

    private Profile setProfileImageURL(Profile profile) throws IOException {
        var imageURL = imageService.downloadImage(profile.getProfileImage().getImageName()).getURL();

        profile.getProfileImage().setImageName(imageURL.toString());

        return profile;
    }
}
