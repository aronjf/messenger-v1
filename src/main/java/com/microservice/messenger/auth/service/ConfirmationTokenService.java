package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.ConfirmationToken;

public interface ConfirmationTokenService {

    void saveConfirmationToken(ConfirmationToken confirmationToken);

    ConfirmationToken saveConfirmationToken(String associatedEmail);

    ConfirmationToken findConfirmationTokenById(String id);
}
