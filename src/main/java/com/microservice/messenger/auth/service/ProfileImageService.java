package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.ProfileImage;

import java.io.IOException;

public interface ProfileImageService {

    ProfileImage saveProfileImage(ProfileImage profileImage);

    ProfileImage saveImage(ProfileImage profileImage) throws IOException;
}
