package com.microservice.messenger.auth.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;

public interface ImageService {

    Resource uploadImage(MultipartFile multipartFile) throws IOException;

    Resource downloadImage(String imageName) throws MalformedURLException;
}
