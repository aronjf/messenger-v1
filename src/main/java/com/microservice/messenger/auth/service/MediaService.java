package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.User;

import java.util.List;

public interface MediaService {

    List<User> getOnlineUsers();
}
