package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.CountryOfResidence;

public interface CountryOfResidenceService {

    void saveCountryOfResidence(CountryOfResidence countryOfResidence);

    void updateCountryOfResidence(CountryOfResidence countryOfResidence);
}
