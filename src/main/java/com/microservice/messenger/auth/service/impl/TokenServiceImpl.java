package com.microservice.messenger.auth.service.impl;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.exception.JwtTokenNotValidException;
import com.microservice.messenger.auth.model.entity.Role;
import com.microservice.messenger.auth.model.entity.Token;
import com.microservice.messenger.auth.repository.TokenRepository;
import com.microservice.messenger.auth.service.TokenService;
import com.microservice.messenger.auth.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterable;
import java.util.stream.Collectors;

import static com.microservice.messenger.auth.filter.util.FilterProperties.*;

@Service
@AllArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final UserService userService;
    private final TokenRepository tokenRepository;

    @Logback
    @Override
    public Map<String, String> checkFilter(String authHeader, String requestUrl) throws JwtTokenNotValidException, EntityNotFoundException {
        if (authHeader.isEmpty() || !authHeader.startsWith(TOKEN_PREFIX))
            throw new JwtTokenNotValidException(authHeader);

        var refreshToken = authHeader.substring(TOKEN_PREFIX.length());

        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
        JWTVerifier verifier = JWT.require(algorithm).build();

        DecodedJWT decodedJWT = verifier.verify(refreshToken);

        var accessToken = JWT.create()
                .withSubject(decodedJWT.getSubject())
                .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                .withIssuer(requestUrl)
                .withClaim("roles", userService
                        .findUserByUsername(decodedJWT.getSubject())
                        .getRoles()
                        .stream()
                        .map(Role::getRoleName)
                        .collect(Collectors.toList()))
                .sign(algorithm);

        Map<String, String> tokens = new HashMap<>();

        tokens.put(ACCESS_TOKEN, accessToken);

        return tokens;
    }

    @Logback
    @Override
    public void saveToken(String token) {
        tokenRepository.save(new Token(token));
    }

    @Override
    public DecodedJWT decodeToken(String token) {
        Algorithm algorithm = Algorithm.HMAC256(SECRET_KEY.getBytes(StandardCharsets.UTF_8));
        JWTVerifier verifier = JWT.require(algorithm).build();

        return verifier.verify(token);
    }

    @Logback
    @Override
    public Iterable<Token> getAllTokens() {
        return tokenRepository.findAll();
    }

    @Logback
    @Override
    public String getUsernameByVerifiedToken(String token) {
        try {
            var decodedToken = decodeToken(token);

            return decodedToken.getSubject();
        } catch(TokenExpiredException e){
            return null;
        }
    }

    @Logback
    @Scheduled(fixedDelayString = "PT10M")
    void clearTokenRepositoryFromExpiredTokens() {
        var tokens = tokenRepository.findAll();

        tokens.forEach(s -> {
            try{
                decodeToken(s.getId());
            } catch (TokenExpiredException e){
                tokenRepository.delete(s);
            }
        });
    }
}
