package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.model.entity.CountryOfResidence;
import com.microservice.messenger.auth.repository.CountryOfResidenceRepository;
import com.microservice.messenger.auth.service.CountryOfResidenceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CountryOfResidenceServiceImpl implements CountryOfResidenceService {

    private final CountryOfResidenceRepository countryOfResidenceRepository;

    @Override
    public void saveCountryOfResidence(CountryOfResidence countryOfResidence) {
        countryOfResidenceRepository.save(countryOfResidence);
    }

    @Override
    public void updateCountryOfResidence(CountryOfResidence countryOfResidence) {
        countryOfResidenceRepository.updateCountryOfResidence(countryOfResidence);
        countryOfResidenceRepository.updateIsEmptyStatus(countryOfResidence.getCountryOfResidenceId());
    }
}
