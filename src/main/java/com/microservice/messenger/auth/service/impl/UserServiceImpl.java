package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.repository.UserRepository;
import com.microservice.messenger.auth.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Logback
    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Logback
    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    @Logback
    @Override
    public User findUserByUsername(String username) throws EntityNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(
                () -> new EntityNotFoundException(username)
        );
    }

    @Logback
    @Override
    public User saveUser(User user) {
        return userRepository.save(Objects.requireNonNull(user));
    }

    @Logback
    @Override
    public void updateUser(User user) {
        userRepository.updateUser(user.getUserId());
    }

    @Override
    public User findUserByUserId(Long userId) throws EntityNotFoundException {
        return userRepository.findByUserId(userId).orElseThrow(
                () -> new EntityNotFoundException(userId)
        );
    }
}
