package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.model.entity.Role;
import com.microservice.messenger.auth.repository.RoleRepository;
import com.microservice.messenger.auth.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Logback
    @Override
    public Role findRoleByName(String name) {
        return roleRepository.findByRoleName(name);
    }
}
