package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.Profile;
import com.microservice.messenger.auth.model.entity.User;

import java.io.IOException;

public interface ProfileService {

    Profile findProfileByUser(User user);

    void saveProfile(Profile profile);

    Profile updateProfile(Profile profile) throws IOException;

    Profile findProfileByUserId(Long userId) throws EntityNotFoundException, IOException;

    Profile findProfileByUsername(String username) throws EntityNotFoundException, IOException;
}
