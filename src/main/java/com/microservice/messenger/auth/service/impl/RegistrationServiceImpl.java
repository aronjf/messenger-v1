package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.model.entity.*;
import com.microservice.messenger.auth.service.*;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.microservice.messenger.auth.model.Roles.USER;

@Service
@AllArgsConstructor
public class RegistrationServiceImpl implements RegistrationService {

    private final UserService userService;
    private final RoleService roleService;
    private final ProfileService profileService;
    private final PasswordEncoder passwordEncoder;

    @Logback
    @Override
    public User saveUser(User newUser) throws EntityExistException {
        if(userService.findByUsername(newUser.getUsername()).isPresent())
            throw new EntityExistException(newUser.getUsername());

        var user = User.builder()
                .username(newUser.getUsername())
                .email(newUser.getEmail())
                .password(passwordEncoder.encode(newUser.getPassword()))
                .roles(List.of(roleService.findRoleByName(USER.name())))
                .isEnabled(false)
                .build();

        var aboutInformation = AboutInformation.builder()
                .countryOfResidence(new CountryOfResidence())
                .build();

        var profile = Profile.builder()
                .aboutInformation(aboutInformation)
                .profileImage(new ProfileImage())
                .user(user)
                .build();

        user = userService.saveUser(user);

        profileService.saveProfile(profile);

        return user;
    }

    @Logback
    @Override
    public void verifyUser(String email) {
        userService.updateUser(userService.findByEmail(email).get());
    }
}
