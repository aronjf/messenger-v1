package com.microservice.messenger.auth.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.exception.JwtTokenNotValidException;
import com.microservice.messenger.auth.model.entity.Token;

import java.util.Map;

public interface TokenService {

    Map<String, String> checkFilter(String authHeader, String requestUrl) throws JwtTokenNotValidException, EntityNotFoundException;

    void saveToken(String token);

    DecodedJWT decodeToken(String token);

    Iterable<Token> getAllTokens();

    String getUsernameByVerifiedToken(String token);
}
