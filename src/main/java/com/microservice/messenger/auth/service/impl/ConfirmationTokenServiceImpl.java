package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.model.entity.ConfirmationToken;
import com.microservice.messenger.auth.repository.ConfirmationTokenRepository;
import com.microservice.messenger.auth.service.ConfirmationTokenService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.UUID;

@Service
@AllArgsConstructor
public class ConfirmationTokenServiceImpl implements ConfirmationTokenService {
    private final ConfirmationTokenRepository confirmationTokenRepository;

    @Logback
    @Override
    public void saveConfirmationToken(ConfirmationToken confirmationToken) {
        confirmationTokenRepository.save(confirmationToken);
    }

    @Logback
    @Override
    public ConfirmationToken saveConfirmationToken(String associatedEmail) {
        ConfirmationToken confirmationToken = ConfirmationToken.builder()
                .id(UUID.randomUUID().toString())
                .associatedEmail(associatedEmail)
                .build();

        return confirmationTokenRepository.save(confirmationToken);
    }

    @Logback
    @Override
    public ConfirmationToken findConfirmationTokenById(String id) {
        return confirmationTokenRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(id));
    }
}
