package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.Role;

public interface RoleService {

    Role findRoleByName(String name);
}
