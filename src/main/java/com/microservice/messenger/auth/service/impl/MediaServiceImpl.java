package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.service.MediaService;
import com.microservice.messenger.auth.service.TokenService;
import com.microservice.messenger.auth.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class MediaServiceImpl implements MediaService {

    private final TokenService tokenService;
    private final UserService userService;

    @Logback
    @Override
    public List<User> getOnlineUsers() {
        var tokens = tokenService.getAllTokens();

        List<User> users = new ArrayList<>();

        tokens.forEach(s -> {
            var username = tokenService.getUsernameByVerifiedToken(s.getId());

            if(username == null)
                return;

            var user = userService.findUserByUsername(username);

            users.add(user);
        });

        return users;
    }
}
