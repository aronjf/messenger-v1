package com.microservice.messenger.auth.service.impl;

import com.microservice.messenger.auth.annotation.Logback;
import com.microservice.messenger.auth.exception.RequiredMediaTypePngException;
import com.microservice.messenger.auth.model.entity.ProfileImage;
import com.microservice.messenger.auth.repository.ProfileImageRepository;
import com.microservice.messenger.auth.service.ImageService;
import com.microservice.messenger.auth.service.ProfileImageService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;

@Service
@AllArgsConstructor
public class ProfileImageServiceImpl implements ProfileImageService {

    private final ProfileImageRepository profileImageRepository;
    private final ImageService imageService;

    @Logback
    @Override
    public ProfileImage saveProfileImage(ProfileImage profileImage) {
        return profileImageRepository.save(profileImage);
    }

    @Logback
    @Override
    public ProfileImage saveImage(ProfileImage profileImage) throws IOException {
        var contentType = Objects.requireNonNull(
                profileImage.getMultipartFile().getContentType());

        if(!contentType.equals(MediaType.IMAGE_PNG_VALUE))
            throw new RequiredMediaTypePngException(profileImage.getImageName(), contentType);

        var hashedImageName = String.valueOf(
                Objects.requireNonNull(
                        profileImage.getMultipartFile().getOriginalFilename()).hashCode()
        );

        profileImage.setImageName(hashedImageName);

        var updatedProfileImage = saveProfileImage(profileImage);

        var imageURL = imageService.uploadImage(profileImage.getMultipartFile()).getURL();

        updatedProfileImage.setImageName(imageURL.toString());

        return updatedProfileImage;
    }
}
