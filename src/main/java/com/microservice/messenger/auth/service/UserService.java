package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.User;

import java.util.Optional;

public interface UserService {

    Optional<User> findByUsername(String username);

    Optional<User> findByEmail(String email);

    User findUserByUsername(String username) throws EntityNotFoundException;

    User saveUser(User user);

    void updateUser(User user);

    User findUserByUserId(Long userId) throws EntityNotFoundException;
}
