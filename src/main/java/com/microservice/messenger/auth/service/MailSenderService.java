package com.microservice.messenger.auth.service;

public interface MailSenderService {
    void send(String to, String emailText);

    String buildEmailText(String userName, String token);
}
