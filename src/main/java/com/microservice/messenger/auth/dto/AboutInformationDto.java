package com.microservice.messenger.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AboutInformationDto {

    private Long aboutId;

    private String dateOfBirth;

    private String aboutUser;

    private CountryOfResidenceDto countryOfResidence;
}
