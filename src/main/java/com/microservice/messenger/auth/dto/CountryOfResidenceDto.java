package com.microservice.messenger.auth.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CountryOfResidenceDto {

    private Long countryOfResidenceId;

    private String country;

    private String town;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private boolean isEmpty;
}
