package com.microservice.messenger.auth.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.microservice.messenger.messenger.dto.MessageDto;
import lombok.*;
import org.springframework.context.annotation.PropertySource;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@PropertySource("classpath:/application_additional.properties")
public class UserDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Long userId;

    @NotBlank
    @Size(
            min = 2,
            max = 15,
            message = "{warnings.validation.username}")
    private String username;

    @NotBlank
    @Size(
            min = 5,
            max = 30,
            message = "{warnings.validation.password}"
    )
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    @Email(message = "{warnings.validation.email}")
    private String email;

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = MessageDto.DATE_PATTERN)
    private LocalDateTime passedIn = LocalDateTime.now();
}
