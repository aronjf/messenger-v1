package com.microservice.messenger.auth.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.microservice.messenger.messenger.dto.MessageDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UploadProfileImageDto {

    private Integer imageId;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    String imageName;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    MultipartFile multipartFile;

    @Builder.Default
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @JsonFormat(pattern = MessageDto.DATE_PATTERN)
    LocalDateTime dateOfImage = LocalDateTime.now();
}
