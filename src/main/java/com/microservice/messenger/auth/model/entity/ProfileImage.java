package com.microservice.messenger.auth.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.time.LocalDateTime;

@DynamicInsert
@Entity
@Table(name = "media_profile_image")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProfileImage {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "image_id")
    @SequenceGenerator(name = "image_id", sequenceName = "media_profile_image__id_seq", allocationSize = 1)
    @Column(name = "id")
    private Integer imageId;

    @Column(name = "image_name",
            columnDefinition = "varchar(255) default '720991618'")
    private String imageName;

    @Transient
    private MultipartFile multipartFile;

    @Column(name = "date_of_image",
            columnDefinition = "timestamp default current_timestamp")
    private LocalDateTime dateOfImage;
}
