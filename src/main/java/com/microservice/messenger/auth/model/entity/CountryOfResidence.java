package com.microservice.messenger.auth.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@DynamicInsert
@Entity
@Table(name = "country_of_residence")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CountryOfResidence {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long countryOfResidenceId;

    private String country;

    private String town;

    @Column(name = "is_empty",
            columnDefinition = "boolean default false")
    private boolean isEmpty;
}
