package com.microservice.messenger.auth.model.entity;

import lombok.*;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;

@RedisHash("token")
@Data
@Builder
@AllArgsConstructor
public class Token {

    @Id
    private String id;
}
