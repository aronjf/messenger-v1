package com.microservice.messenger.auth.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;

@DynamicInsert
@Entity
@Table(name = "media_about_information")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AboutInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long aboutId;

    @Column(name = "date_of_birth")
    private String dateOfBirth;

    @Column(name = "about_user",
            columnDefinition = "varchar(255) default 'The user has chosen not to leave any mention of himself'")
    private String aboutUser;

    @OneToOne(cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH
    }, fetch = FetchType.EAGER)
    @JoinColumn(name = "country_of_residence_id")
    private CountryOfResidence countryOfResidence;
}
