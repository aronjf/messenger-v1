package com.microservice.messenger.auth.model;

public enum Roles {
    USER,
    ADMIN,
    CREATOR
}
