package com.microservice.messenger.auth.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;

@RedisHash("confirmation_token")
@Data
@Builder
@AllArgsConstructor
public class ConfirmationToken {

    @Id
    private String id;

    private String associatedEmail;
}
