package com.microservice.messenger.auth.model.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "role_")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer roleId;

    @Column(name = "role_name")
    private String roleName;
}
