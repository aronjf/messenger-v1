package com.microservice.messenger.auth.config;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RoleExpression {

    public static final String PERMIT_ALL = "permitAll()";
    public static final String AUTHENTICATED = "isAuthenticated()";
    public static final String ROLE_ADMIN = "hasRole('ADMIN')";
    public static final String ROLE_USER = "hasRole('USER')";
    public static final String AUTHORITY_CREATOR = "hasAuthority('CREATOR')";
}
