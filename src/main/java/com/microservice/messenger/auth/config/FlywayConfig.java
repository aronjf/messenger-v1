package com.microservice.messenger.auth.config;

import lombok.RequiredArgsConstructor;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

import javax.sql.DataSource;

@Configuration
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class FlywayConfig {

    @Value("${spring.flyway.locations}")
    String locations;

    @Value("${spring.flyway.callBackLocation}")
    String callBackLocation;

    @Value("${spring.flyway.schemas}")
    String flywaySchemas;

    final DataSource dataSource;

    @EventListener(ApplicationReadyEvent.class)
    public void migrate() {
        var fluentConfiguration = new FluentConfiguration();

        fluentConfiguration.dataSource(dataSource)
                .baselineOnMigrate(true)
                .table("messaging_schema_history")
                .schemas(flywaySchemas)
                .locations(locations,callBackLocation);

        var flyway = new Flyway(fluentConfiguration);

        flyway.migrate();
    }
}
