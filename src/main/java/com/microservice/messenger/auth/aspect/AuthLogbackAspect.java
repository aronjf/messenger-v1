package com.microservice.messenger.auth.aspect;

import com.microservice.messenger.messenger.dto.MessageDto;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Aspect
@Slf4j
@Component
public class AuthLogbackAspect {

    public static final String ANNOTATION_POINTCUT_LOGBACK = "@annotation(com.microservice.messenger.auth.annotation.Logback)";
    public static final String ANNOTATION_POINTCUT_LOGBACK_MESSAGE = "@annotation(com.microservice.messenger.messenger.annotation.LogbackMessage)";
    public static final String POINTCUT = "logbackPointcut()";
    public static final String POINTCUT_MESSAGE = "logbackMessagePointcut()";

    @Pointcut(ANNOTATION_POINTCUT_LOGBACK)
    public void logbackPointcut(){
    }

    @Pointcut(ANNOTATION_POINTCUT_LOGBACK_MESSAGE)
    public void logbackMessagePointcut(){
    }

    @Before(POINTCUT)
    public void logbackMethodBefore(JoinPoint joinPoint){
        var methodName = joinPoint.getSignature().getName();

        log.debug("logbackMethodBefore is running...");
        log.info("Executing method: {}", methodName);
    }

    @AfterThrowing(value = ANNOTATION_POINTCUT_LOGBACK, throwing = "e")
    public void logbackException(JoinPoint joinPoint, Throwable e){
        log.debug("logbackException is running...");
        log.error("Method {} exception: {}", joinPoint.getSignature().getName(), e.getMessage());
    }

    @AfterReturning(value = ANNOTATION_POINTCUT_LOGBACK, returning = "object")
    public void logbackReturning(JoinPoint joinPoint, Object object){
        var methodName = joinPoint.getSignature().getName();

        log.debug("LogbackReturning is running...");
        log.info("Method executed: {} with {}", methodName, object != null ? object.toString() : "null");
    }

    @AfterReturning(value = ANNOTATION_POINTCUT_LOGBACK_MESSAGE, returning = "object")
    public void logbackMessageReturning(JoinPoint joinPoint, Object object){
        var methodName = joinPoint.getSignature().getName();
        var date = LocalDateTime.now().format(DateTimeFormatter.ofPattern(MessageDto.DATE_PATTERN));

        log.debug("LogbackMessageReturning is running...");
        log.info("Message {} sent in {} with method name {}", object, date, methodName);
    }
}
