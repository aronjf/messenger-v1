package com.microservice.messenger.auth.filter;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.microservice.messenger.auth.service.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Objects;

import static com.microservice.messenger.auth.filter.util.FilterProperties.*;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Component
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class AuthorizationFilter extends OncePerRequestFilter {

    private final TokenService tokenService;

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if(request.getServletPath().equals(LOGIN_ENDPOINT)
                || request.getServletPath().equals(REGISTRATION_ENDPOINT)
                || request.getServletPath().equals(REFRESH_ENDPOINT)
                || request.getServletPath().matches(CONFIRMATION_ENDPOINT)) {
            filterChain.doFilter(request, response);

            return;
        }

        String authHeader = request.getHeader(AUTHORIZATION);

        if (Objects.isNull(authHeader) && !authHeader.startsWith(TOKEN_PREFIX))
            filterChain.doFilter(request, response);

        String token = authHeader.substring(TOKEN_PREFIX.length());

        DecodedJWT decodedJWT = tokenService.decodeToken(token);

        String username = decodedJWT.getSubject();
        Collection<SimpleGrantedAuthority> authorities = decodedJWT.getClaim("roles").asList(SimpleGrantedAuthority.class);

        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(username, null, authorities);

        SecurityContextHolder.getContext().setAuthentication(authenticationToken);

        filterChain.doFilter(request, response);
    }
}
