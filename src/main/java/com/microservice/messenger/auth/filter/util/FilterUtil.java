package com.microservice.messenger.auth.filter.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.microservice.messenger.auth.exception.JwtTokenNotValidException;
import com.microservice.messenger.auth.service.TokenService;
import com.microservice.messenger.auth.service.impl.UserDetailsImpl;
import lombok.AllArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.microservice.messenger.auth.filter.util.FilterProperties.ACCESS_TOKEN;
import static com.microservice.messenger.auth.filter.util.FilterProperties.SECRET_KEY;

@Component
@AllArgsConstructor
public class FilterUtil {

    private final TokenService tokenService;

    public Map<String, String> generateToken(String requestUrl, UserDetailsImpl user) {
        String accessToken = JWT.create()
                .withSubject(user.getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                .withIssuer(requestUrl)
                .withClaim("roles", user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .sign(Algorithm.HMAC256(SECRET_KEY.getBytes(StandardCharsets.UTF_8)));

        tokenService.saveToken(accessToken);

        Map<String, String> tokens = new HashMap<>();

        tokens.put(ACCESS_TOKEN, accessToken);

        return tokens;
    }
}
