package com.microservice.messenger.auth.filter.util;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class FilterProperties {

    public static final String SECRET_KEY = "nikitaron";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REGISTRATION_ENDPOINT = "/api/user/registration";
    public static final String LOGIN_ENDPOINT = "/api/user/login";
    public static final String REFRESH_ENDPOINT = "/api/user/refresh";
    public static final String CONFIRMATION_ENDPOINT = "\\/api\\/user\\/registration\\/[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}";
}
