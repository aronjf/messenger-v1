create table user_(
                      id       bigint        not null,
                      username varchar(255)  not null unique,
                      email    varchar(255)  not null,
                      password varchar(255)  not null,
                      is_enabled boolean     not null default false,
                      primary key(id)
);

create table role_(
                      id        smallint     not null,
                      role_name varchar(255) not null,
                      primary key(id)
);

create table users_role(
                           user_id bigint,
                           role_id smallint,
                           foreign key(user_id) references user_(id),
                           foreign key(role_id) references role_(id)
);

create table country_of_residence(
                                     id           bigint     not null,
                                     country      varchar(50),
                                     town         varchar(50),
                                     is_empty boolean default true not null,
                                     primary key(id)
);

create table media_about_information(
                                        id                      bigint   not null,
                                        date_of_birth           varchar(255),
                                        about_user              varchar(255) default 'The user has chosen not to leave any mention of himself',
                                        country_of_residence_id bigint not null,
                                        primary key(id),
                                        foreign key(country_of_residence_id) references country_of_residence(id)
);

create sequence media_profile_image__id_seq start 1 increment 1;

create table media_profile_image(
                                    id                 smallint default nextval('media_profile_image__id_seq') not null,
                                    image_name         varchar(255) default '720991618',
                                    date_of_image      timestamp default current_timestamp,
                                    primary key(id)
);

create table media_profile(
                              id                   bigint       not null,
                              user_id              bigint       not null,
                              image_id             smallint     not null,
                              about_information_id bigint       not null,
                              primary key(id),
                              foreign key(user_id)              references user_(id),
                              foreign key(image_id)             references media_profile_image(id),
                              foreign key(about_information_id) references media_about_information(id)
);