package com.microservice.messenger.util;

public class Util {

    public static class ForTesting{
        public final static String USERNAME = "nikitaron";
        public final static String EMAIL = "username@gmail.com";
        public final static String PASSWORD = "password";
        public final static String CHATROOM = "chatroom";
        public final static String MESSAGE = "message";
        public final static String URL = "http://localhost:3031/api/user/refresh";
        public final static String COUNTRY = "Belarus";
        public final static String TOWN = "Minsk";
        public final static String DATE_OF_BIRTH = "01/01/2002";
        public final static String IMAGE_NAME = "720991618";
        public final static String USER = "USER";
        public final static String ADMIN = "ADMIN";
    }

    public static class ContentType{
        public final static String APPLICATION_JSON = "application/json";
    }
}
