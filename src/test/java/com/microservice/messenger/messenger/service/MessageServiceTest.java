package com.microservice.messenger.messenger.service;

import com.microservice.messenger.messenger.model.entity.Message;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class MessageServiceTest {

    @Autowired
    private MessageService messageService;

    @Nested
    @Disabled("Do tests when logic of method would be rewritten")
    @DisplayName("Send message to chat room tests")
    class sendMessageToChatRoom{

        @Test
        @DisplayName("Send message to chat room (must pass)")
        void sendMessageToChatRoom_mustPass() {
            var message = Message.builder()
                    .chatRoomId(UUID.randomUUID().toString())
                    .message(Util.ForTesting.MESSAGE)
                    .build();

            assertDoesNotThrow(() -> messageService.sendMessageToChatRoom(message));
        }
    }
}