package com.microservice.messenger.messenger.service;

import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.repository.UserRepository;
import com.microservice.messenger.messenger.model.entity.ChatRoom;
import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import com.microservice.messenger.messenger.repository.ChatRoomRepository;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ChatRoomServiceTest {

    @Autowired
    private ChatRoomService chatRoomService;

    @MockBean
    private ChatRoomRepository chatRoomRepository;

    @MockBean
    private UserRepository userRepository;

    @Nested
    @DisplayName("Save chat room tests")
    class saveChatRoomTests{

        @Test
        @DisplayName("Save chat room when chat room not exists in database (must pass)")
        void saveChatRoom_mustPass_whenChatRoomNotExistsInDatabase() {
            var chatRoom = ChatRoom.builder()
                    .id(UUID.randomUUID().toString())
                    .chatRoomName(Util.ForTesting.CHATROOM)
                    .build();

            var user = User.builder()
                    .username(Util.ForTesting.USERNAME)
                    .email(Util.ForTesting.EMAIL)
                    .password(Util.ForTesting.PASSWORD)
                    .build();

            when(chatRoomRepository.findChatRoomByChatRoomName(Util.ForTesting.CHATROOM)).thenReturn(Optional.empty());
            when(userRepository.findByUsername(Util.ForTesting.USERNAME)).thenReturn(Optional.ofNullable(user));
            assertDoesNotThrow(() -> chatRoomService.saveChatRoom(chatRoom, Util.ForTesting.USERNAME));
        }

        @Test
        @DisplayName("Save chat room when chat room exists in database (must throw exception)")
        void saveChatRoom_mustThrowEntityExistException_whenChatRoomExistsInDatabase() {
            var chatRoom = ChatRoom.builder()
                    .id(UUID.randomUUID().toString())
                    .chatRoomName(Util.ForTesting.CHATROOM)
                    .build();

            when(chatRoomRepository.findChatRoomByChatRoomName(Util.ForTesting.CHATROOM)).thenReturn(Optional.ofNullable(chatRoom));
            assertThrows(EntityExistException.class, () -> chatRoomService.saveChatRoom(chatRoom, Util.ForTesting.USERNAME));
        }
    }

    @Nested
    @Disabled("Do tests when logic of method would be rewritten")
    @DisplayName("Drop chat room tests")
    class dropChatRoomTests {

        @Test
        @DisplayName("Drop chat room when chat room exists in database (must pass)")
        void dropChatRoom_mustPass_whenChatRoomExistsInDatabase() {

        }

        @Test
        @DisplayName("Drop chat room when chat room not exists in database (must throw exception)")
        void dropChatRoom_mustThrowChatRoomNotFoundException_whenChatRoomNotExistsInDatabase(){

        }
    }

    @Test
    @Disabled("Do tests when logic of method would be rewritten")
    @DisplayName("Join chat room (must pass)")
    void joinChatRoom_mustPass() throws EntityNotFoundException {
        var chatRoomUser = ChatRoomUser.builder()
                .username(Util.ForTesting.USERNAME)
                .build();

        var chatRoom = ChatRoom.builder()
                .chatRoomName(Util.ForTesting.CHATROOM)
                .build();

        List<ChatRoomUser> connectedUsers = new ArrayList<>();

        connectedUsers.add(chatRoomUser);

        var expectedChatRoom = ChatRoom.builder()
                .chatRoomName(Util.ForTesting.CHATROOM)
                .connectedUsers(connectedUsers)
                .build();

        when(chatRoomRepository.save(chatRoom)).thenReturn(null);
        assertEquals(expectedChatRoom, chatRoomService.joinChatRoom(chatRoomUser, chatRoom));
    }

    @Nested
    @DisplayName("Find chat room by id tests")
    class findChatRoomByIdTests{

        @Test
        @DisplayName("Find chat room by id when chat room exists in database (must pass)")
        void findChatRoomById_mustPass_whenChatRoomExistsInDatabase() {
            var chatRoom = ChatRoom.builder().build();

            var id = UUID.randomUUID().toString();

            when(chatRoomRepository.findById(id)).thenReturn(Optional.of(chatRoom));
            assertDoesNotThrow(() -> chatRoomService.findChatRoomById(id));
        }

        @Test
        @DisplayName("Find chat room by id when chat room not exists in database (must throw exception)")
        void findChatRoomById_mustThrowEntityNotFoundException_whenChatRoomNotExistsInDatabase(){
            var id = UUID.randomUUID().toString();

            when(chatRoomRepository.findById(id)).thenReturn(Optional.empty());
            assertThrows(EntityNotFoundException.class, () -> chatRoomService.findChatRoomById(id));
        }
    }
}