package com.microservice.messenger.messenger.service;

import com.microservice.messenger.messenger.model.entity.ChatRoomUser;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ChatRoomUserServiceTest {

    @Autowired
    private ChatRoomUserService chatRoomUserService;

    @Test
    @DisplayName("Find user by username and build chat room user (must pass)")
    void findUserByUsernameAndBuildChatRoomUser_mustPass() {
        var date = LocalDateTime.now();
        var expectedChatRoomUser = ChatRoomUser.builder()
                .username(Util.ForTesting.USERNAME)
                .joinAt(date)
                .build();

        var chatRoomUser = chatRoomUserService
                .findUserByUsernameAndBuildChatRoomUser(Util.ForTesting.USERNAME);
        chatRoomUser.setJoinAt(date);

        assertEquals(expectedChatRoomUser, chatRoomUser);
    }
}