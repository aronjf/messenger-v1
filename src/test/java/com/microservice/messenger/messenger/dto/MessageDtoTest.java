package com.microservice.messenger.messenger.dto;

import com.microservice.messenger.util.Util;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class MessageDtoTest {

    @Test
    @DisplayName("Test equals and hashcode")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple()
                .forClass(MessageDto.class)
                .verify();
    }

    @Test
    @DisplayName("Test toString")
    void testToString() {

        var instantTime = LocalDateTime.now();

        var messageDto = MessageDto.builder()
                .message(Util.ForTesting.MESSAGE)
                .fromUser(Util.ForTesting.USERNAME)
                .toUser(Util.ForTesting.USERNAME)
                .sendingDate(instantTime)
                .build()
                .toString();

        var expectedMessageDto = "MessageDto(chatRoomId=null, message=message, fromUser=nikitaron, toUser=nikitaron, " +
                "sendingDate=" + instantTime +")";

        assertEquals(expectedMessageDto, messageDto);
    }
}