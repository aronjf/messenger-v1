package com.microservice.messenger.messenger.dto;

import com.microservice.messenger.util.Util;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class ChatRoomDtoTest {

    @Test
    @DisplayName("Test equals and hashcode")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple()
                .forClass(ChatRoomDto.class)
                .verify();
    }

    @Test
    @DisplayName("Test toString")
    void testToString() {

        var chatRoomDto = ChatRoomDto.builder()
                .chatRoomName(Util.ForTesting.CHATROOM)
                .connectedUser(Collections.emptyList())
                .build()
                .toString();

        var expectedChatRoomDto = "ChatRoomDto(id=null, chatRoomName=chatroom, connectedUser=[])";

        assertEquals(expectedChatRoomDto, chatRoomDto);
    }
}