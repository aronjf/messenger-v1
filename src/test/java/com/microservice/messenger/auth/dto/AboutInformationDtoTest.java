package com.microservice.messenger.auth.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AboutInformationDtoTest {

    @Test
    @DisplayName("Test equals and hashcode")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple()
                .forClass(AboutInformationDto.class)
                .verify();
    }
}