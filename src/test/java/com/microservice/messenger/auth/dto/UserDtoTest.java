package com.microservice.messenger.auth.dto;

import com.microservice.messenger.util.Util;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

class UserDtoTest {

    @Test
    @DisplayName("Test equals and hashcode")
    void testEqualsAndHashCode() {
        EqualsVerifier.simple()
                .forClass(UserDto.class)
                .verify();
    }

    @Test
    @DisplayName("Test toString")
    void testToString() {

        var instantTime = LocalDateTime.now();

        var userDto = UserDto.builder()
                 .userId(1L)
                .username(Util.ForTesting.USERNAME)
                .email(Util.ForTesting.EMAIL)
                .passedIn(instantTime)
                .password(Util.ForTesting.PASSWORD)
                .build()
                .toString();

        var expectedUserDto = "UserDto(userId=1, username=nikitaron, password=password, email=username@gmail.com, " +
                "passedIn=" + instantTime + ")";

        Assertions.assertEquals(expectedUserDto, userDto);
    }
}