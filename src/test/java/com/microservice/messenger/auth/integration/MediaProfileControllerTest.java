package com.microservice.messenger.auth.integration;

import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.exception.RequiredMediaTypePngException;
import com.microservice.messenger.auth.filter.AuthorizationFilter;
import com.microservice.messenger.auth.model.entity.*;
import com.microservice.messenger.auth.service.ProfileImageService;
import com.microservice.messenger.auth.service.ProfileService;
import com.microservice.messenger.auth.service.UserService;
import com.microservice.messenger.util.Util;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.Principal;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@ExtendWith(MockitoExtension.class)
class MediaProfileControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthorizationFilter authorizationFilter;

    @MockBean
    private Principal principal;

    @MockBean
    private ProfileImageService profileImageService;

    @MockBean
    private ProfileService profileService;

    @MockBean
    private UserService userService;

    private ProfileImage profileImage;

    private User user;

    private AboutInformation aboutInformation;

    private CountryOfResidence countryOfResidence;

    private Profile profile;

    @BeforeEach
    void setUp() throws ServletException, IOException {
        mockMvc = MockMvcBuilders.webAppContextSetup(context)
                .alwaysDo(print())
                .build();

        RestAssuredMockMvc.mockMvc(mockMvc);

        doNothing().when(authorizationFilter).doFilterInternal(
                any(HttpServletRequest.class),
                any(HttpServletResponse.class),
                any(FilterChain.class));

        user = User.builder()
                .userId(1L)
                .username(Util.ForTesting.USERNAME)
                .roles(Collections.emptyList())
                .build();

        countryOfResidence = CountryOfResidence.builder()
                .countryOfResidenceId(1L)
                .country(Util.ForTesting.COUNTRY)
                .town(Util.ForTesting.TOWN)
                .isEmpty(false)
                .build();

        aboutInformation = AboutInformation.builder()
                .aboutId(1L)
                .aboutUser("The user has chosen not to leave any mention of himself")
                .countryOfResidence(countryOfResidence)
                .build();

        profileImage = ProfileImage.builder()
                .imageId(1)
                .build();

        profile = Profile.builder()
                .profileId(1L)
                .user(user)
                .aboutInformation(aboutInformation)
                .profileImage(profileImage)
                .build();
    }

    @Nested
    @DisplayName("User profile tests")
    class UserProfileTests{

        @Test
        @DisplayName("User profile when user exists in database (must pass)")
        @WithMockUser(username = Util.ForTesting.USERNAME, authorities = Util.ForTesting.USER)
        void userProfile_mustPass_whenUserExistsInDatabase() throws EntityNotFoundException, IOException {
            when(profileService.findProfileByUserId(1L)).thenReturn(profile);

            RestAssuredMockMvc
                    .when()
                    .get("/api/media/profile/1")
                    .then()
                    .expect(jsonPath("$.profileId").value(1))
                    .expect(jsonPath("$.user[?(@.userId == 1)]").exists())
                    .expect(jsonPath("$.user[?(@.username)]").isNotEmpty())
                    .expect(jsonPath("$.profileImage[?(@.imageId == 1)]").exists())
                    .expect(jsonPath("$.profileImage[?(@.imageName)]").isNotEmpty())
                    .expect(jsonPath("$.aboutInformation[?(@.aboutUser == 'The user has chosen not to leave any mention of himself')]").exists())
                    .statusCode(200)
                    .contentType(Util.ContentType.APPLICATION_JSON);
        }

        @Test
        @DisplayName("User profile when user not exists in database (must throw exception)")
        @WithMockUser(username = Util.ForTesting.USERNAME, authorities = Util.ForTesting.USER)
        void userProfile_mustThrowException_whenUserNotExistsInDatabase() throws EntityNotFoundException, IOException {
            when(profileService.findProfileByUserId(100L)).thenThrow(new EntityNotFoundException(100L));

            RestAssuredMockMvc
                    .when()
                    .get("/api/media/profile/100")
                    .then()
                    .statusCode(400)
                    .statusLine("400 Entity 100 not found");
        }
    }

    @Test
    @WithMockUser(username = Util.ForTesting.USERNAME, authorities = Util.ForTesting.ADMIN)
    void usersProfile_mustPass_whenUserAuthenticated() throws EntityNotFoundException, IOException {
        when(profileService.findProfileByUsername(Util.ForTesting.USERNAME)).thenReturn(profile);
        when(principal.getName()).thenReturn(Util.ForTesting.USERNAME);

        RestAssuredMockMvc.authentication = RestAssuredMockMvc.principal(principal);

        RestAssuredMockMvc
                .when()
                  .get("/api/media/profile/me")
                .then()
                  .expect(jsonPath("$.profileId").value(1))
                  .expect(jsonPath("$.user[?(@.userId == 1)]").exists())
                  .expect(jsonPath("$.user[?(@.username)]").isNotEmpty())
                  .expect(jsonPath("$.profileImage[?(@.imageId == 1)]").exists())
                  .expect(jsonPath("$.profileImage[?(@.imageName)]").isNotEmpty())
                  .expect(jsonPath("$.aboutInformation[?(@.aboutUser == 'The user has chosen not to leave any mention of himself')]").exists())
                  .statusCode(200)
                  .contentType(Util.ContentType.APPLICATION_JSON);
    }

    @Nested
    @DisplayName("Update users profile tests")
    class UpdateUsersProfileTests {

        @Test
        @DisplayName("Update users profile when update is not null (must pass)")
        @WithMockUser(username = Util.ForTesting.USERNAME, authorities = Util.ForTesting.ADMIN)
        void updateUsersProfile_mustPass_whenUpdateIsNotNull() throws IOException, EntityNotFoundException {
            aboutInformation.setAboutUser("Some update");
            countryOfResidence.setTown("Vitebsk");

            when(principal.getName()).thenReturn(Util.ForTesting.USERNAME);
            when(userService.findUserByUsername(anyString())).thenReturn(user);
            when(profileService.updateProfile(any(Profile.class))).thenReturn(profile);

            RestAssuredMockMvc.authentication = RestAssuredMockMvc.principal(principal);

            RestAssuredMockMvc
                    .given()
                    .contentType(Util.ContentType.APPLICATION_JSON)
                    .body("{\n" +
                            "    \"aboutInformation\": {\n" +
                            "        \"aboutId\": 1,\n" +
                            "        \"dateOfBirth\": \"12 01 2010\",\n" +
                            "        \"aboutUser\": \"Some update\",\n" +
                            "        \"countryOfResidence\": {\n" +
                            "            \"countryOfResidenceId\": 1,\n" +
                            "            \"country\": \"Belarus\",\n" +
                            "            \"town\": \"Vitebsk\"\n" +
                            "        }\n" +
                            "    }\n" +
                            "}")
                    .when()
                    .put("/api/media/profile/me")
                    .then()
                    .expect(jsonPath("$.profileId").value(1))
                    .expect(jsonPath("$.aboutInformation[?(@.aboutUser == 'Some update')]").isNotEmpty())
                    .expect(jsonPath("$.aboutInformation.countryOfResidence[?(@.town == 'Vitebsk')]").isNotEmpty())
                    .expect(jsonPath("$.aboutInformation.countryOfResidence[?(@.isEmpty == false)]").exists())
                    .statusCode(200)
                    .contentType(Util.ContentType.APPLICATION_JSON);
        }

        @Test
        @DisplayName("Update users profile when update is null (must pass)")
        @WithMockUser(username = Util.ForTesting.USERNAME, authorities = Util.ForTesting.ADMIN)
        void updateUsersProfile_mustPass_whenUpdateIsNull() throws IOException, EntityNotFoundException {
            when(principal.getName()).thenReturn(Util.ForTesting.USERNAME);
            when(userService.findUserByUsername(anyString())).thenReturn(user);
            when(profileService.updateProfile(any(Profile.class))).thenReturn(profile);

            RestAssuredMockMvc.authentication = RestAssuredMockMvc.principal(principal);

            RestAssuredMockMvc
                    .given()
                    .contentType(Util.ContentType.APPLICATION_JSON)
                    .body("{\n" +
                            "    \"aboutInformation\": {\n" +
                            "        \"aboutId\": 1,\n" +
                            "        \"dateOfBirth\": \"12 01 2010\",\n" +
                            "        \"aboutUser\": null,\n" +
                            "        \"countryOfResidence\": {\n" +
                            "            \"countryOfResidenceId\": 1,\n" +
                            "            \"country\": null,\n" +
                            "            \"town\": null\n" +
                            "        }\n" +
                            "    }\n" +
                            "}")
                    .when()
                    .put("/api/media/profile/me")
                    .then()
                    .expect(jsonPath("$.profileId").value(1))
                    .expect(jsonPath("$.aboutInformation[?(@.aboutUser == 'The user has chosen not to leave any mention of himself')]").exists())
                    .expect(jsonPath("$.aboutInformation.countryOfResidence[?(@.country == 'Belarus')]").isNotEmpty())
                    .expect(jsonPath("$.aboutInformation.countryOfResidence[?(@.town == 'Minsk')]").isNotEmpty())
                    .statusCode(200)
                    .contentType(Util.ContentType.APPLICATION_JSON);
        }
    }

    @Nested
    @DisplayName("Upload image tests")
    class UploadImageTests{

        @Test
        @DisplayName("Upload image when file is png (must pass)")
        @WithMockUser(username = Util.ForTesting.USERNAME, roles = Util.ForTesting.USER)
        void uploadImage_mustPass_mustPass_whenFileIsPng() throws Exception {
            var file = new File("src/test/resources/images/720991618");

            var multipartFile = new MockMultipartFile(
                    "720991618",
                    "720991618.png",
                    MediaType.IMAGE_PNG_VALUE,
                    Files.readAllBytes(file.toPath()));

            when(profileImageService.saveImage(any(ProfileImage.class))).thenReturn(any(ProfileImage.class));

            mockMvc
                    .perform(multipart("/api/media/profile/upload")
                            .file("multipartFile", multipartFile.getBytes())
                            .param("imageId", String.valueOf(1)))
                    .andExpect(status().isOk());
        }

        @Test
        @DisplayName("Upload image when file is not png (must throw exception)")
        @WithMockUser(username = Util.ForTesting.USERNAME, roles = Util.ForTesting.USER)
        void uploadImage_mustThrowRequiredMediaTypePngException_whenFileIsNotPng() throws Exception {
            var multipartFile = new MockMultipartFile(
                    "message",
                    "message.txt",
                    MediaType.TEXT_PLAIN_VALUE,
                    Util.ForTesting.MESSAGE.getBytes(StandardCharsets.UTF_8));

            when(profileImageService.saveImage(any(ProfileImage.class))).thenThrow(RequiredMediaTypePngException.class);

            mockMvc
                    .perform(multipart("/api/media/profile/upload")
                            .file("multipartFile", multipartFile.getBytes())
                            .param("imageId", String.valueOf(1)))
                    .andExpect(status().isBadRequest());
        }
    }
}