package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.exception.EntityExistException;
import com.microservice.messenger.auth.model.entity.*;
import com.microservice.messenger.auth.repository.RoleRepository;
import com.microservice.messenger.auth.repository.UserRepository;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.CallsRealMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static com.microservice.messenger.auth.model.Roles.USER;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class RegistrationServiceTest {

    @Autowired
    private RegistrationService registrationService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private RoleRepository roleRepository;

    @MockBean
    private UserService userService;

    @MockBean
    private ProfileService profileService;

    Role role;

    @BeforeEach
    void setUp(){
        role = Role.builder()
                .roleId(1)
                .roleName(USER.name())
                .build();
    }

    @Nested
    @DisplayName("Save user entity tests")
    class saveUserTests{

        @Test
        @DisplayName("Save user when entity not exist (must pass)")
        void saveUser_mustPass_whenEntityNotExists() throws EntityExistException, IOException {
            var user = User.builder()
                    .username(Util.ForTesting.USERNAME)
                    .email(Util.ForTesting.EMAIL)
                    .password(Util.ForTesting.PASSWORD)
                    .build();

            var expectedUser = User.builder()
                    .userId(1L)
                    .username(Util.ForTesting.USERNAME)
                    .email(Util.ForTesting.EMAIL)
                    .password(new BCryptPasswordEncoder().encode(Util.ForTesting.PASSWORD))
                    .roles(Collections.singletonList(role))
                    .isEnabled(true)
                    .build();

            var profile = Profile.builder()
                    .aboutInformation(new AboutInformation())
                    .profileImage(new ProfileImage())
                    .user(user)
                    .build();

            when(userRepository.save(user)).thenReturn(user);
            when(roleRepository.findByRoleName(USER.name())).thenReturn(role);
            when(userService.saveUser(any(User.class))).thenReturn(expectedUser);
            doNothing().when(profileService).saveProfile(profile);
            assertEquals(expectedUser, registrationService.saveUser(user));
        }

        @Test
        @DisplayName("Save user when entity exist (must throw exception)")
        void saveUser_mustThrowEntityExistException_whenEntityExist(){
            var user = User.builder()
                    .userId(1L)
                    .username(Util.ForTesting.USERNAME)
                    .build();

            when(userService.findByUsername(anyString())).thenReturn(Optional.of(user));
            assertThrows(EntityExistException.class, () -> registrationService.saveUser(user));
        }
    }
}