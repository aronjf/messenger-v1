package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.*;
import com.microservice.messenger.auth.repository.ProfileRepository;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ProfileServiceTest {

    @MockBean
    private ProfileRepository profileRepository;

    @MockBean
    private ProfileImageService profileImageService;

    @MockBean
    private AboutInformationService aboutInformationService;

    @Autowired
    private UserService userService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ProfileService profileService;

    @Test
    @DisplayName("Find profile by user when user exists (must pass)")
    void findProfileByUser_mustPass_whenUserExistsInDatabase() {
        var user = User.builder().build();

        when(profileRepository.findByUser(user)).thenReturn(any());
        assertDoesNotThrow(() -> profileService.findProfileByUser(user));
    }

    @Test
    @DisplayName("Save profile when profile is not null (must pass)")
    void saveProfile_mustPass_whenProfileIsNotNull() {
        var profile = Profile.builder().build();

        when(profileRepository.save(profile)).thenReturn(any());
        assertDoesNotThrow(() -> profileService.saveProfile(profile));
    }

    @Test
    void findProfileByUserId() {
    }

    @Test
    void findProfileByUsername() {
    }
}