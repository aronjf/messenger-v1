package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.repository.UserRepository;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    @Nested
    @DisplayName("Find user by username tests")
    class findUserByUsernameTests{

        @Test
        @DisplayName("Find user by username when user exists (must pass)")
        void findUserByUsername_mustPass_whenUserExistsInDatabase() {
            var user = User.builder().userId(1L).build();

            when(userRepository.findByUsername(Util.ForTesting.USERNAME)).thenReturn(Optional.ofNullable(user));
            assertDoesNotThrow(() -> userService.findUserByUsername(Util.ForTesting.USERNAME));
        }

        @Test
        @DisplayName("Find user by username when user not exists (must throw exception)")
        void findUserByUsername_mustThrowEntityNotFoundException_whenUserNotExistsInDatabase(){
            assertThrows(EntityNotFoundException.class, () -> userService.findUserByUsername(Util.ForTesting.USERNAME));
        }

        @Test
        @DisplayName("Find user by username when user exists (must pass and return optional)")
        void findByUsername_mustPass_whenUserExistsInDatabase(){
            when(userRepository.findByUsername(Util.ForTesting.USERNAME)).thenReturn(Optional.empty());
            assertDoesNotThrow(() -> userService.findByUsername(Util.ForTesting.USERNAME));
        }
    }

    @Test
    @DisplayName("Save user (must pass)")
    void saveUser_mustPass_whenUserNotExistsInDatabase() {
        var user = User.builder().build();

        assertDoesNotThrow(() -> userService.saveUser(user));
    }
}