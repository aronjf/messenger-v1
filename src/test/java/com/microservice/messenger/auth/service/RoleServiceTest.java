package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.Role;
import com.microservice.messenger.auth.repository.RoleRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static com.microservice.messenger.auth.model.Roles.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class RoleServiceTest {

    @Autowired
    private RoleService roleService;

    @MockBean
    private RoleRepository roleRepository;

    @Nested
    @DisplayName("Find role by name tests")
    class findRoleByName{

        @Test
        @DisplayName("Find role by name (must pass)")
        void findRoleByName_mustPass_whenRoleExistsInDatabase() {
            Role expectedRole = Role.builder()
                    .roleId(1)
                    .roleName(USER.name())
                    .build();

            when(roleRepository.findByRoleName(anyString())).thenReturn(expectedRole);
            assertEquals(expectedRole, roleService.findRoleByName(USER.name()));
        }

        @Test
        @DisplayName("Find role by name (must throw exception)")
        void findRoleByName_mustThrowNullPointerException_whenRoleNotExistsInDatabase() {
            when(roleRepository.findByRoleName(anyString())).thenReturn(null);
            when(roleService.findRoleByName(anyString())).thenThrow(NullPointerException.class);
        }
    }
}