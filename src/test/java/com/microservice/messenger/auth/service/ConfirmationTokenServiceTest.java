package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.ConfirmationToken;
import com.microservice.messenger.auth.repository.ConfirmationTokenRepository;
import com.microservice.messenger.auth.service.impl.ConfirmationTokenServiceImpl;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ConfirmationTokenServiceTest {

    @Autowired
    private ConfirmationTokenService confirmationTokenService;

    @MockBean
    private ConfirmationTokenRepository confirmationTokenRepository;

    @Test
    void saveToken_mustPass_whenTokenSaved() {
        var confirmationToken = ConfirmationToken.builder()
                .associatedEmail(Util.ForTesting.EMAIL)
                .build();

        when(confirmationTokenRepository.save(any(ConfirmationToken.class))).thenReturn(confirmationToken);
        assertDoesNotThrow(() -> confirmationTokenService.saveConfirmationToken(confirmationToken));
    }

    @Test
    void findTokenById_mustPass() {
        var randomUUID = UUID.randomUUID().toString();

        var confirmationToken = ConfirmationToken.builder()
                .associatedEmail(Util.ForTesting.EMAIL)
                .build();

        when(confirmationTokenRepository.findById(randomUUID)).thenReturn(Optional.ofNullable(confirmationToken));
        assertDoesNotThrow(() -> confirmationTokenService.findConfirmationTokenById(randomUUID));
    }
}
