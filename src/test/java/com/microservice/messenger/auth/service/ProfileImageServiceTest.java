package com.microservice.messenger.auth.service;

import com.microservice.messenger.auth.model.entity.ProfileImage;
import com.microservice.messenger.auth.repository.ProfileImageRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class ProfileImageServiceTest {

    @MockBean
    private ProfileImageRepository profileImageRepository;

    @Autowired
    private ProfileImageService profileImageService;

    @Test
    @DisplayName("Save profile image (must pass)")
    void saveProfileImage_mustPass() {
        var profileImage = ProfileImage.builder().build();

        when(profileImageRepository.save(profileImage)).thenReturn(any());
        assertDoesNotThrow(() -> profileImageService.saveProfileImage(profileImage));
    }
}