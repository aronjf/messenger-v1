package com.microservice.messenger.auth.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.microservice.messenger.auth.exception.EntityNotFoundException;
import com.microservice.messenger.auth.exception.JwtTokenNotValidException;
import com.microservice.messenger.auth.model.entity.Role;
import com.microservice.messenger.auth.model.entity.Token;
import com.microservice.messenger.auth.model.entity.User;
import com.microservice.messenger.auth.repository.TokenRepository;
import com.microservice.messenger.util.Util;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.microservice.messenger.auth.filter.util.FilterProperties.SECRET_KEY;
import static com.microservice.messenger.auth.filter.util.FilterProperties.TOKEN_PREFIX;
import static com.microservice.messenger.auth.model.Roles.USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class TokenServiceTest {

    @Mock
    private TokenService tokenServiceMock;

    @Autowired
    private TokenService tokenService;

    @MockBean
    private TokenRepository tokenRepository;

    @MockBean
    private UserService userService;

    @Nested
    @DisplayName("Check filter tests")
    class checkFilterTests{

        @Test
        @DisplayName("Check filter when token is not valid (must throw exception)")
        void checkFilter_mustThrowJwtTokenNotValidException_whenTokenNotValid() throws JwtTokenNotValidException, EntityNotFoundException {
            assertThrows(JwtTokenNotValidException.class, () -> tokenService.checkFilter("Bearer", Util.ForTesting.URL));
        }

        @Test
        @DisplayName("Check filter when token is valid (must pass)")
        void checkFilter_mustPass_whenTokenIsValid() throws EntityNotFoundException {
            var role = Role.builder()
                    .roleId(1)
                    .roleName(USER.name())
                    .build();

            List<Role> roles = new ArrayList<>();

            roles.add(role);

            var user = User.builder()
                    .username(Util.ForTesting.USERNAME)
                    .roles(roles)
                    .build();

            var token = JWT.create()
                    .withSubject(Util.ForTesting.USERNAME)
                    .withExpiresAt(new Date(System.currentTimeMillis() + 10 * 60 * 1000))
                    .withIssuer(Util.ForTesting.URL)
                    .withClaim("roles", roles.stream().map(Role::getRoleName).collect(Collectors.toList()))
                    .sign(Algorithm.HMAC256(SECRET_KEY.getBytes(StandardCharsets.UTF_8)));

            var authorizationToken = new StringBuffer(token).insert(0, TOKEN_PREFIX);

            when(userService.findUserByUsername(Util.ForTesting.USERNAME)).thenReturn(user);
            assertDoesNotThrow(() -> tokenService.checkFilter(authorizationToken.toString(), Util.ForTesting.URL));
        }
    }

    @Nested
    @DisplayName("Decode token tests")
    class decodeTokenTests{

        @Test
        @DisplayName("Decode token when token has expired (must throw exception)")
        void decodeToken_mustPass_whenTokenNotExpiredAndValid() {
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrc3VzaGFhYWFhYWEiLCJyb2xlcyI6WyJVU0VSIl0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAzMS9hcGkvdXNlci9sb2dpbiIsImV4cCI6MTY0MjA4NDAxMn0.1IPgVCz5-a0h8oNwAz4l3ecHCatl5boV1JsxI5xU6xI";

            when(tokenServiceMock.decodeToken(anyString())).thenReturn(any());
            assertDoesNotThrow(() -> tokenServiceMock.decodeToken(token));
        }

        @Test
        @DisplayName("Decode token when token has expired (must throw exception)")
        void decodeToken_mustThrowTokenExpiredException_whenTokenHasExpired() {
            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJrc3VzaGFhYWFhYWEiLCJyb2xlcyI6WyJVU0VSIl0sImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6MzAzMS9hcGkvdXNlci9sb2dpbiIsImV4cCI6MTY0MjA4NDAxMn0.1IPgVCz5-a0h8oNwAz4l3ecHCatl5boV1JsxI5xU6xI";

            assertThrows(TokenExpiredException.class, () -> tokenService.decodeToken(token));
        }
    }
    
    @Test
    @DisplayName("Save token (must pass)")
    void saveToken_mustPass() {
        Token token = Token.builder().build();

        when(tokenRepository.save(any())).thenReturn(token);
        assertDoesNotThrow(() -> tokenService.saveToken(Util.ForTesting.CHATROOM));
    }
}